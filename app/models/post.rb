class Post < ApplicationRecord
    belongs_to :user
    validates :title, uniqueness: { case_sensitive: false },
                        presence: true,
                        length: {maximum: 128}
end
