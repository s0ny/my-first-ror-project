class PostsController < ApplicationController
  before_action :authenticate_user!

  def index
    @posts = Post.all
  end

  def new
    @post = current_user.posts.build
  end

  def create
    @post = current_user.posts.build(post_params)    
    if @post.save
      flash[:notice] = "Data berhasil disimpan"
      redirect_to posts_path
    else
      render "new"
    end
  end

  def edit
    @post = current_user.posts.find(params[:id])
  end

  def update
    @post = current_user.posts.find(params[:id]) 
    if @post.update(post_params)
      flash[:notice] = "Data berhasil diubah"
      redirect_to posts_path
    else
      render "edit"
    end
  end

  def show
  end

  def destroy
    @post = Post.find(params[:id])
    @post.destroy
  end

  private
  def post_params
    params.require(:post).permit(:title, :body)
  end
end
