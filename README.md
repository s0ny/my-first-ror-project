# README

Aplikasi ini dibuat menggunakan:
* Ruby versi 2.2
* Rails versi 5.1.5
* MariaDB versi 10.2.7
* Node.js 8.10.0

Proses installasi aplikasi:
* Pastikan Ruby, Rails, Node.js dan MariaDB telah terinstall
* Clone atau download aplikasi ini
* Sesuaikan User, Password dan NamaDB dengan file 
  ```
  /config/database.yml
  ```
* Installasi gem yg dibutuhkan aplikasi
  ```
  bundle install
  ```  
* Installasi tabel-tabel Db
  ```
  rails db:migrate
  ```  
* Memulai web server
  ```
  rails s --environment=test
  ```  
* Buka aplikasi di browser dengan alamat localhost:3000